# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-fastavro
_pkgname=fastavro
pkgver=1.9.2
pkgrel=0
pkgdesc="Fast Avro for Python"
# Tests for optional zstd and snappy codecs require
# unpackaged python modules 'zstandard' and 'python-snappy'
options="!check"
url="https://github.com/fastavro/fastavro"
arch="all !x86" # _tz_ tests fail
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-setuptools python3-dev cython py3-wheel"
checkdepends="py3-pytest-xdist py3-numpy"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastavro/fastavro/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	FASTAVRO_USE_CYTHON=1 \
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH="$(echo $PWD/build/lib.*)" python3 -m pytest -v tests
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
1f63b0bb23311da5108f6058f426de44db85e4c04ce1e36cc2a9f6af4536a8cee7ad4087053731d04fc0a2f783bcd1684d7a5c178bd1ed5957763afcb70565be  py3-fastavro-1.9.2.tar.gz
"
