# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=perl-mime-tools
_realname=MIME-tools
pkgver=5.511
pkgrel=0
pkgdesc="Perl modules for parsing (and creating!) MIME entities"
url="https://metacpan.org/dist/MIME-tools"
arch="noarch"
license="GPL-1.0-or-later OR Artistic-1.0-Perl"
depends="perl perl-io-stringy perl-mailtools perl-convert-binhex"
checkdepends="perl-test-deep perl-test-pod"
subpackages="$pkgname-doc"
source="https://cpan.metacpan.org/authors/id/D/DS/DSKOLL/$_realname-$pkgver.tar.gz"
builddir="$srcdir/$_realname-$pkgver"

prepare() {
	default_prepare

	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
}

build() {
	export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete
}

sha512sums="
1d22458d8ee82f7bcdcebadbc70684781ad090a5a6f03fb0032e6cdfa463c23b70e66449a38561fb139d99a19b7c72f6154da7b794ecfd44b39abeaf011f55fa  MIME-tools-5.511.tar.gz
"
