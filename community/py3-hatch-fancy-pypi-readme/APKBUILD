# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-hatch-fancy-pypi-readme
_pkgname=${pkgname/py3-/}
pkgver=23.2.0
pkgrel=0
pkgdesc="Fancy PyPI READMEs with Hatch"
url="https://github.com/hynek/hatch-fancy-pypi-readme"
arch="noarch"
license="MIT"
makedepends="py3-gpep517 py3-installer py3-hatchling py3-hatch-vcs"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/hynek/hatch-fancy-pypi-readme/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -k 'not test_end_to_end.py'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
eb678326fc6bc440c1bd64513ca235c628eaac73c16d9272c160acc8aa116076d8ac5bcdec7176c1299d5598c3025a2dce78a17a07c456ff2973245cdf722a7e  py3-hatch-fancy-pypi-readme-23.2.0.tar.gz
"
